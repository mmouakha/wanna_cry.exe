#!/bin/bash
matrice()
{
echo "Trouver l'imposteur"
n=$((1+$RANDOM%50))
m=$((1+$RANDOM%100))
posy=$((1+$RANDOM%$n))
posx=$((1+$RANDOM%$m))
i=$((1))
while [ $i -ne $(($n+1)) ]
do
	if [ "$(((n-i+1)%10))" -eq "$((0))" ]
        then
                echo -n "_   "
        else
                echo -n "    "
        fi
	j=$((1))
	while [ $j -ne $(($m+1)) ]
	do
		if [ ! $i = $posy ] || [ ! $j = $posx ]
		then 
			echo -n $1
		else
			echo -n $2
		fi
		((j++))
	done
	((i++))
	echo ""
done
echo ""
echo -n "    "
k=$((1))
while [ $k -ne $(($m+1)) ]
do
        if [ "$((k%10))" -eq "$((0))" ]
        then
                echo -n "|"
        else
                echo -n " "
        fi
        ((k++))
done
echo ""
echo "La matrice est de taille $n x $m"
echo "Donner l'abscisse de l'imposteur (de gauche à droite, 1 marqueur toutes les 10 colonnes)"
#echo $posx
read repx
echo "Donner l'ordonnée de l'imposteur (de bas en haut, 1 marqueur toutes les 10 lignes)"
#echo $posy
usery=$(($n-$posy+1))
#echo "A rentrer : $usery"
read repy
realy=$(($n-$repy+1))
#echo $realy
if [ $repx -eq $posx ] && [ $realy -eq $posy ]
then
	echo "Well Done"
	exit
else
	echo "Try Again"
	xterm -geometry 300x300+0+0 -e "./imposteur.sh"
fi
}
configuration()
{
config=$(($RANDOM%12))
if [ $config -eq 0 ]
then
	matrice l I
elif [ $config -eq 1 ]
then
	matrice I l
elif [ $config -eq 2 ]
then
	matrice g q
elif [ $config -eq 3 ]
then
	matrice q g
elif [ $config -eq 4 ]
then
	matrice v w
elif [ $config -eq 5 ]
then
	matrice w v
elif [ $config -eq 6 ]
then
        matrice n m
elif [ $config -eq 7 ]
then
        matrice m n
elif [ $config -eq 8 ]
then
        matrice k h
elif [ $config -eq 9 ]
then
        matrice h k
elif [ $config -eq 10 ]
then
        matrice j i
elif [ $config -eq 11 ]
then
        matrice i j
fi
}
configuration
