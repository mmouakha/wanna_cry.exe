enigmes=8

while [ $enigmes -gt 0 ]
do
	echo "Il vous reste" $enigmes "enigme(s) à passer pour débloquer l'ordinateur"
	
	
	#lancer une enigme ici
#---------------------------------------------------------------------------	
	num_prog=$((1+$RANDOM%8))
	cd wanna_cry.exe/

		
	if [ $num_prog -eq 1 ]
	then cd parking/
		xterm -geometry 300x300+0+0 -e "./script.sh"
		cd ..
	
	elif [ $num_prog -eq 2 ]
	then cd enigmes_textuelles
		xterm -geometry 300x300+0+0 -e "./enigmes.sh banque_geo.txt"
		cd ..
	
	elif [ $num_prog -eq 3 ]
	then cd enigmes_textuelles
		xterm -geometry 300x300+0+0 -e "./enigmes.sh banque_histoire.txt"
		cd ..
	
	elif [ $num_prog -eq 4 ]
	then cd enigmes_textuelles
		xterm -geometry 300x300+0+0 -e "./enigmes.sh banque_math"
		cd ..

	elif [ $num_prog -eq 5 ]
	then cd enigmes_textuelles
		xterm -geometry 300x300+0+0 -e "./enigmes.sh banque_geo.txt"
		cd ..
		
	elif [ $num_prog -eq 6 ]
	then cd memo/
		xterm -geometry 300x300+0+0 -e "./memo.sh"
		cd ..

	elif [ $num_prog -eq 7 ]
	then xterm -geometry 300x300+0+0 -e "./imposteur.sh"
		cd ..
	elif [ $num_prog -eq 8 ]
	then cd labyrinthe
        	 xterm -geometry 300x300+0+0 -e "./labyrinthe.py"
                cd ..

	fi
#---------------------------------------------------------------------------		


	if [ $? = 0 ] #la valeur renvoyée par les programmes
		then
			enigmes=$(( $enigmes - 1 ))
	fi
done


if enigmes=0
	then echo "
	
	
	
	
	
	
	
Vous avez résolus toutes les enigmes, vous pouvez récupérer le contrôle de votre ordinateur !
	
	
	
	
	
	
	
	"

fi
