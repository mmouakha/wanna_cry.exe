#!/bin/bash

cat affichage.sh
echo "Quel est le numéro de la place à laquelle la voiture est garée ? Attention vous n'avez que 5 essais..."

read number
essai=4

while [ $number != 97 ] && [ $essai != 0 ]
do
	echo "Mauvaise réponse...Try again ! Il vous reste " $essai "essai(s) "
	read number
	essai=$(( $essai - 1 ))

done

if [ $number = 97 ]
then
	echo "Oui c'est ça ! Bravo vous avez trouvé :) !"
	val_exit=0
else
	echo "Vous n'avez pas réussi l'enigme :'( NOOBS !"
	val_exit=1
fi
	
exit $val_exit
