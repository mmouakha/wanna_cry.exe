#!/bin/bash

param=$1
if [ $param != 0 ]
then
	figlet "You  have  been  hacked"
	if [ $param -ge 10 ]
	then
		n1=$((($param-10)*500))
		n2=0
	elif [ $param -ge 6 ]
	then
		n1=$((($param-6)*500))
		n2=350
	else
		n1=$((($param)*500))
		n2=1000
	fi
	xterm -geometry +$(($n1))+$(($n2)) -e "./debut.sh $(($param-1))"
else
	xterm -e "./jeu_principal/script_jeu.sh; sleep 50"
fi
