from tkinter import *
from random import randrange

def move():
    global x
    global y,pX,pY
    global Personnage
    can.delete('all')
    quadrillages()
    i=len(Personnage)-1
    j=0
    while i > 0:   #Deplacement du Personnage - Version Serpent
        Personnage[i][0]=Personnage[i-1][0]
        Personnage[i][1]=Personnage[i-1][1]
        can.create_rectangle(Personnage[i][0], Personnage[i][1], Personnage[i][0] +10, Personnage[i][1]+10,outline='blue', fill='black')
        i=i-1
#Déplacement du Personnage / boucle si sortie du script
        if collision(Personnage[0]):
            sortie()
        if reussite(Personnage[0]):
            print("")
            print("")
            print("Bravo !")
            fen.destroy()
    if direction  == 'gauche':
        Personnage[0][0]  = Personnage[0][0] - dx
        if Personnage[0][0] < 0 :
            sortie()
    elif direction  == 'droite':
        Personnage[0][0]  = Personnage[0][0] + dx
        if Personnage[0][0] > 493 :
            sortie()
    elif direction  == 'haut':
        Personnage[0][1]  = Personnage[0][1] - dy
        if Personnage[0][1] < 0:
            sortie()
    elif direction  == 'bas':
        Personnage[0][1]  = Personnage[0][1] + dy
        if Personnage[0][1] > 493:
            sortie()
    can.create_rectangle(Personnage[0][0], Personnage[0][1], Personnage[0][0]+10, Personnage[0][1]+10,outline='red', fill='red')


    if flag != 0:
        fen.after(120, move)



def newGame():
    global flag
    if flag == 0:
        flag = 1
    move()

def sortie():
    print ("Vous avez perdu. Gros naze !")
    print("")
    print("")
    fen.destroy()

def reussite(Personnage):
    if Personnage[0] > 0 and Personnage[0] < 50 and Personnage[1] > 450 and Personnage[1] < 493:
        return True

def collision(personnage):
    if personnage in carres_infectes:
        return True

def left(event):
    global direction
    direction = 'gauche'

def right(event):
    global direction
    direction = 'droite'

def up(event):
    global direction
    direction = 'haut'

def down(event):
    global direction
    direction = 'bas'


x = 250
y = 250
dx, dy = 10, 10
flag = 0
direction = 'haut'
Personnage=[[x,y],[x+2.5,y+2.5],[x+5,y+5],[0,0]]


#fonction spirale -- donne les points interdits
def spiral(point,lignes):
    X=[point[0]]
    Y=[point[1]]
    Z=[point]
    n=4
    for k in range (1,lignes):
        for i in range (1,n):
            point[1]+=10
            X.append(point[0])
            Y.append(point[1])
            Z.append([point[0],point[1]])
        for i in range(1,n+1):
            point[0]+=10
            point[1]+=10
            X.append(point[0])
            Y.append(point[1])
            Z.append([point[0],point[1]])
        for i in range (1,n+2):
            point[0]+=10
            point[1]-=10
            X.append(point[0])
            Y.append(point[1])
            Z.append([point[0],point[1]])
        for i in range (1,n+3):
            point[1]-=10
            X.append(point[0])
            Y.append(point[1])
            Z.append([point[0],point[1]])
        for i in range (1,n+4):
            point[0]-=10
            point[1]-=10
            X.append(point[0])
            Y.append(point[1])
            Z.append([point[0],point[1]])
        for i in range (1,n+5):
            point[0]-=10
            point[1]+=10
            X.append(point[0])
            Y.append(point[1])
            Z.append([point[0],point[1]])
        n=n+6
    return  Z

#graphique-spirale
global carres_infectes
carres_infectes=spiral([250,250],4)

#Interface graphique - quadrillages
def quadrillages():
    carres_infectes=spiral ([250,250],4)
    for i in range(0,500,10):
        can.create_line(i, 0, i, 500, width=0.01, fill="blue")
        can.create_line(0, i, 500, i, width=0.01, fill="blue")
    for j in range (len(carres_infectes)-2):
        can.create_rectangle(carres_infectes[j][0], carres_infectes[j][1], carres_infectes[j][0]+10, carres_infectes[j][1]+10, outline="white", fill="white")
    can.create_rectangle(0,493,50,450,outline="red", fill="red")


#Debut interface
fen = Tk()
can = Canvas(fen, width=500, height=500, bg='black')
quadrillages()
can.pack(side=TOP, padx=5, pady=5)


#Création objet
rectangle = can.create_rectangle(Personnage[0][0], Personnage[0][1], Personnage[0][0]+10, Personnage[0][1]+10, outline='red', fill='red')


#Boutons
b1 = Button(fen, text='Lancer', command=newGame, bg='black' , fg='blue')
b1.pack(side=LEFT, padx=5, pady=5)

b2 = Button(fen, text='Quitter', command=fen.destroy, bg='black' , fg='blue')
b2.pack(side=RIGHT, padx=5, pady =5)

tex1 = Label(fen, text="Bienvenue au labyrinthe ", bg='black' , fg='blue')
tex1.pack(padx=0, pady=11)

#Commandes
fen.bind('<d>', right)
fen.bind('<q>', left)
fen.bind('<z>' , up)
fen.bind('<s>', down)

fen.mainloop()
