#!/bin/bash

banque=$1

echo "Voyons voir si vous savez résoudre ces énigmes (sans déterminant ni accent)"

nb_enigme=$(cat $banque | wc -l)

while [ -z $bon ] || [ $bon != '1' ]
do
	nb_0_tot=$((RANDOM %$(($nb_enigme/2))))
	ligne_q=$(($nb_0_tot*2 + 1))p
	ligne_r=$(($nb_0_tot*2 + 2))p
	question=$(sed -n $ligne_q $banque)
	read -p "$question " rep
	rep_maj=$(echo "$rep" | tr '[:lower:]' '[:upper:]')
	if [ "$rep_maj" = "$(sed -n $ligne_r $banque)" ]
	then
		echo "Bonne réponse"
		bon=1
	else
	echo "Mauvaise réponse, serez-vous répondre à celle-ci ?"

	fi
done
