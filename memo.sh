#!/bin/bash

#variables
n_images=2

#fonction qui choisit une image
function choix_image {
	i=$(($RANDOM%$n_images))
	return $i
}

#fonction qui affiche et kill une image
function affiche_memo {
	echo image_memo_"$1".jpg
	eog image_memo_"$1".jpg &
	pid_image=$!
	sleep 1
	kill $pid_image
}

#fonction qui demande ce qui a été vu
function demande_memo {
	if [ $1 -eq 0 ]
	then
		liste_objects=( "pomme" "etoile" "avocat" "cle" "sucette" "diamant" "coeur" "steack" "fraise" )
		nb_objects=9
	else
		liste_objects=( "violon" "trompette" "micro" "cd" "loupe" "casque" )
		nb_objects=6
	fi

	for j in $(seq 0 $(($nb_objects-1)))
	do
		liste_objects_joueur[$j]=0
	done
	clear
	echo "Ecrire le nom d'un objet vu et valider (en minuscules, sans espaces, sans déterminant et sans accents) y / Y permet de confirmer la liste."
	i=1
	read object[$i]
	while [ ${object[$i]} != "y" ]  && [ ${object[$i]} != "Y" ]
	do
			trouve=0
			for j in $(seq 0 $(($nb_objects-1)))
			do
				if [ ${object[$i]} = ${liste_objects[$j]} ]
				then
					liste_objects_joueur[$j]=1
					trouve=1
				fi
			done
			if [ $trouve -eq 0 ]
			then
				echo "C'est perdu, la liste d'objects ne contenait pas" ${object[$i]}
				echo "La liste était :"
				for j in $(seq 0 $(($nb_objects-1)))
				do
					echo ${liste_objects[$j]}
				done
				return 1
			fi
			clear
			echo "Liste des objects déjà entrés :"
			for j in $(seq 1 $i)
			do
				echo ${object[$j]}
			done
			echo "Ecrire le nom d'un autre objet vu et valider (en minuscules, sans espaces, sans déterminant et sans accents) y / Y permet de confirmer la liste."
			i=$(($i+1))
			read object[$i]
		done

		for j in $(seq 0 $(($nb_objects-1)))
		do
			if [ ${liste_objects_joueur[$j]} -eq 0 ]
			then
				echo "C'est perdu, vous n'avez pas trouvé tous les objects"
				echo "La liste était :"
				for j in $(seq 0 $(($nb_objects-1)))
				do
					echo ${liste_objects[$j]}
				done
				return 1
			fi
		done
		echo "C'est gagné, la liste était :"
				for j in $(seq 0 $(($nb_objects-1)))
				do
					echo ${liste_objects[$j]}
				done
				return 0
	}

#Programme principal
clear
echo "Bienvenue au jeu du mémo, une image va apparaitre et vous devrez mémoriser les objects présents sur l'image. (y / Y pour commencer)"
read rep
while [ $rep != "y" ]  && [ $rep != "Y" ]
do
	read rep
done
choix_image
image=$?
affiche_memo $image
demande_memo $image
res=$?
while [ $res -eq 1 ]
do
	echo "Vous devez gagner ce jeu pour passer à la suite (y / Y pour recommencer)"
	read rep
	while [ $rep != "y" ]  && [ $rep != "Y" ]
	do
		read rep
	done
	choix_image
	image=$?
	affiche_memo $image
	demande_memo $image
	res=$?
done
exit 0
